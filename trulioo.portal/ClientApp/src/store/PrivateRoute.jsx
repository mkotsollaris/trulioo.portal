﻿export default ({ component: Component, ...rest }) => {
    return (
        <Route
            {...rest}
            render={props =>
                false ? (
                    <Component {...props} />
                ) : (
                        <div>
                            Redirect!
                    </div>
                    )
            }
        />
    );
}