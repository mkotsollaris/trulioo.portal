﻿import React from 'react';
import { Route } from 'react-router';
import Login from './Login';
export default ({ component: Component, ...rest }) => {
    return (
        <Route
            {...rest}
            render={props =>
                false ? (
                    <Component {...props} />
                ) : (
                        <Route exact path='/' component={Login} />
                    )
            }
        />
    );
}