﻿import React, { Component } from "react";
import { Button, InputGroup, FormControl } from "react-bootstrap";
import axios from 'axios';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Redirect,
    withRouter
} from "react-router-dom";
import { css } from 'emotion';
import logo from './clouds_high_res.png';
//import styled from "@emotion/styled";
import trulioo_bto_logo from './Trulioo_Building_Trust_Online.png';
import ggLogo from './globalgateway_large.png';
import Layout from "./Layout";
import Announcements from "./Announcements";

export default class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: "",
            redirectToReferrer: false
        };
    }

    onChange(key, value) {
        console.log('key', key, 'value', value);

        this.setState({
            [key]: value
        });
    }

    handleClick = event => {
        const announcementURL = '/api/login/login';

        const data = {
            username: this.state.username,
            password: this.state.password
        };
        axios.post(announcementURL, data).then(res => {

            const loginIsSuccessful = JSON.parse(res.request.response);
            console.log('loginIsSuccessful', loginIsSuccessful)
            if (loginIsSuccessful) {
                this.setState({ redirectToReferrer: true });
            }
        });
    }

    render() {

        const divStyle = css`
            background-image: url(${logo});
            background-repeat: no-repeat!important;
            height: 100%;
            max-height: 100%;
            margin: 0;
            background-size: 100% 100%;
            padding: 3rem;
            display: flex;
            align-items: center;
            justify-content: center;
        `;

        const marginTopBottom = css`
            margin-top: 2rem;
            margin-bottom: 2rem;
            text-align: center;
        `;

        const inputWidth = css`
            width: 20rem; 
        `;

        const borderStyle = css`
            border: .3em solid #fff;
            border-radius: .8em;
            background-color: rgba(255,255,255,.5);
            padding: 3rem 2.5em;
            width: 40rem;
            margin-bottom: 10rem;
        `;

        const btnStyle = css`
        width: 33rem;
        `;

        const alignCenter = css`
            text-align: center;
        `;

        const inputStyle = css`
            margin-bottom: 1rem;
            width: 33rem;
        `;

        //let { from } = { from: { pathname: "/" } };
        let { redirectToReferrer } = this.state;

        if (redirectToReferrer) {
            return <Layout>
                <Route path="/" component={Announcements} />
            </Layout>;
        };
        return (
            <div className={divStyle}>
                <div className={borderStyle}>
                    <div className={alignCenter}>
                        <img className={alignCenter} src={trulioo_bto_logo} alt="trulioo_bto_logo" />
                    </div>
                    <div className={marginTopBottom}>
                        <h4>Login to</h4>
                    </div>
                    <div className={marginTopBottom}>
                        <img className={alignCenter} src={ggLogo} alt="ggLogo" />
                    </div>
                    <div >
                        <h5>Username:</h5>
                        <FormControl
                            placeholder="username"
                            aria-label=" username"
                            aria-describedby="basic-addon2"
                            onChange={e => { this.onChange("username", e.target.value) }} className={inputStyle} type="text" name="username"
                        />
                        {/* <Input onChange={e => { this.onChange("username", e.target.value) }} className={usernameStyle} type="text" name="username"></Input> */}
                    </div>
                    <div>
                        <h5>Password:</h5>
                        <FormControl
                            placeholder="password"
                            aria-label=" password"
                            aria-describedby="basic-addon2"
                            onChange={e => { this.onChange("password", e.target.value) }} className={inputStyle} type="password"
                        />
                        {/* <input onChange={e => { this.onChange("password", e.target.value) }} className={inputWidth} type="password"></input> */}
                    </div >
                    <br />
                    <Button onClick={this.handleClick} size="lg" color="primary" className={btnStyle}>Login</Button>
                </div >
            </div>
        );
    }
}