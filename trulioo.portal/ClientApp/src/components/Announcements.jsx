﻿import React from 'react';
import axios from 'axios';

export default class Announcements extends React.Component {

    getAnnouncements = () => {
        const announcementURL = '/api/announcements/getannouncements';
        axios.post(announcementURL).then(res => {
            console.log("RES", res);
            const response = JSON.parse(res.request.response);

            const titles = response.map(element => {
                return element.title;
            })
            const descriptions = response.map(element => {
                return element.description;
            })
            this.setState({
                titles,
                descriptions
            });
        });
    };

    componentDidMount() {
        this.getAnnouncements();
    }

    render() {
        const elements = this.state && this.state.titles && this.state.titles.map((element, index) => {
            return <div>
                <b><h3>{element}</h3></b>
                <h4>{this.state.descriptions[index]}</h4>
                <hr />
            </div>
        })
        return <div>
            <h1>Announcements</h1>
            {elements}
        </div >
    }
}