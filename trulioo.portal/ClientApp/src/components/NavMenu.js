import React from "react";
import { Link } from "react-router-dom";
import { Glyphicon, Nav, Navbar, NavItem } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import "./NavMenu.css";

export default props => (
  <Navbar inverse fixedTop fluid collapseOnSelect>
    <Navbar.Header>
      <Navbar.Brand>
        <Link to={"/"}>Trulioo</Link>
      </Navbar.Brand>
      <Navbar.Toggle />
    </Navbar.Header>
    <Navbar.Collapse>
      <Nav>
        <LinkContainer to={"/login"}>
          <NavItem>
            <Glyphicon glyph="th-list" /> Announcements
          </NavItem>
        </LinkContainer>
      </Nav>
      <Nav>
        <LinkContainer to={"/guide"}>
          <NavItem>
            <Glyphicon glyph="th-list" /> Datasource Guide
          </NavItem>
        </LinkContainer>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
);
