import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter
} from "react-router-dom";
import Login from "./components/Login";
import Layout from "./components/Layout";
import Home from "./components/Home";
import Announcements from "./components/Announcements";
import { css } from "emotion";

// export default () => (
// <Layout>
//     <Route path='/announcements' component={Announcements} />
// </Layout>
// );

//TODO change to styled.div
const heightDivStyle = css`
  height: 100%;
`;

export default () => {
  const isLoggedIn = false;
  if (!isLoggedIn) {
    return (
      <div className={heightDivStyle}>
        <Route path="/login" component={Login} />
        <Route path="/guide" component={Login} />
      </div>
    );
  }
  console.log("perasa");
  return (
    <Layout>
      <Route path="/announcements" component={Announcements} />
    </Layout>
  );
};

// <div className={heightDivStyle} >
//     <Router>
//         <div className={heightDivStyle}>

//             <Route path="/(|login)/" component={LoginExample} />
//             <Route path="/home" component={Protected} />
//             <Route path='/announcements' component={Announcements} />
//         </div>
//     </Router>
// </div>

// const fakeAuth = {
//   isAuthenticated: false,
//   authenticate(cb) {
//     this.isAuthenticated = true;
//     setTimeout(cb, 100); // fake async
//   },
//   signout(cb) {
//     this.isAuthenticated = false;
//     setTimeout(cb, 100);
//   }
// };

// class LoginExample extends Component {
//   state = { redirectToReferrer: false };

//   login = () => {
//     fakeAuth.authenticate(() => {
//       this.setState({ redirectToReferrer: true });
//     });
//   };

//   render() {
//     let { from } = (this.props &&
//       this.props.location &&
//       this.props.location.state) || { from: { pathname: "/" } };

//     let { redirectToReferrer } = this.state;

//     if (redirectToReferrer) return <Redirect to={from} />;

//     return (
//       <div className={heightDivStyle}>
//         <Login />
//       </div>
//     );
//   }
// }

// export default AuthExample;
