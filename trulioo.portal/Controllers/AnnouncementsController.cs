﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using trulioo.portal.Configuration;
using Trulioo.Client.V2.Entities;

namespace trulioo.portal.Controllers
{
    [Route("api/[controller]")]
    public class AnnouncementsController : Controller
    {
        private readonly IConfiguration _configuration;

        public AnnouncementsController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpPost("[action]")]
        public async Task<IEnumerable<Announcement>> GetAnnouncements()
        {
            var sdkConfiguration = new SDKConfiguration(_configuration);
            var loginResult = await sdkConfiguration.Login("menelaos_20_dev_ui", "Sobeys!2345");
            var sdkClient = sdkConfiguration.GetService();

            var announcements = await sdkClient.Configuration.Announcements.GetCurrentAsync();
            return announcements;
        }
    }
}
