﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace trulioo.portal.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        [HttpPost("[action]")]
        public async Task<dynamic> GetUsers(int accountId, int userId)
        {
            return null;
            //            var accountUsers = await SessionProvider.ApiService.Configuration.Users.GetUsersAsync(request.AccountId);
            //
            //            var users = accountUsers.Select(u => new
            //            {
            //                id = u.UserGuid,
            //                accountId = u.AccountId,
            //                creationDate = u.CreationDate,
            //                lastActiveDate = u.LastActivityDate,
            //                name = u.Name,
            //                email = u.Email,
            //                editable = u.Editable,
            //                status = u.Enabled ? "Enabled" : "Disabled",
            //                userType = UserHelper.GetDisplayableRoleName(UserHelper.GeUserType(u.Roles)),
            //                roles = UserHelper.GetUserRoles(u).Where(r => r.IsInRole)
            //                                                  .Select(r => new { name = r.Name, isInRole = r.IsInRole })
            //                                                  .ToArray()
            //            }).ToArray();
            //
            //            return new { users };
            //        }
            //
            //   
            //        public async Task<dynamic> GetAccounts()
            //        {
            //            var currentAccountId = SessionProvider.CurrentAccount.ID;
            //
            //            var accounts = SessionProvider.CurrentAccount.SubAccounts.Select(sa => new
            //            {
            //                id = sa.ID,
            //                name = sa.Name,
            //                isEnabled = sa.IsApproved
            //            }).OrderBy(a => a.name).ToList();
            //
            //            var currentUser = await SessionProvider.ApiService.Configuration.Users.GetCurrentAsync();
            //
            //            return new
            //            {
            //                accounts,
            //                currentAccountId,
            //                canCreateUser = currentUser.Roles.Contains(PortalUserRole.UserManager.ToString())
            //            };
            //        }
            // 
            //        public async Task<dynamic> GetUser([Bind(Include = "AccountId, UserId")]Models.UserRequest request)
            //        {
            //            var accountUser = !request.UserId.HasValue ?
            //                                new User { AccountId = request.AccountId, Roles = new List<string> { PortalUserRole.PortalUser.ToString() } }
            //                               : await SessionProvider.ApiService.Configuration.Users.GetAsync(request.UserId.Value);
            //
            //            var user = new
            //            {
            //                accountId = accountUser.AccountId,
            //                creationDate = accountUser.CreationDate,
            //                lastActiveDate = accountUser.LastActivityDate,
            //                name = accountUser.Name,
            //                email = accountUser.Email,
            //                enabled = accountUser.Enabled,
            //                userType = (int)UserHelper.GeUserType(accountUser.Roles),
            //                roles = UserHelper.GetUserRoles(accountUser),
            //                userGuid = accountUser.UserGuid
            //            };
            //
            //            return new { user };
            //        }
            //
            //        
            //        public async Task<dynamic> CreateUser([Bind(Include = "ChangePasswordUrl")]NewUser user)
            //        {
            //            var request = new CreateUserRequest
            //            {
            //                AccountId = user.AccountId,
            //                ChangePasswordUrl = string.Format("https://{0}/account/changepassword", getRequestUriHost(Request)),
            //                Email = user.Email,
            //                Enabled = user.Enabled,
            //                Name = user.Name,
            //                Roles = (user.Roles ?? new List<string>()).Select(r => ((PortalUserRole)Int32.Parse(r)).ToString()).ToList()
            //            };
            //
            //            try
            //            {
            //                ValidateUser(user.Roles);
            //                await SessionProvider.ApiService.Configuration.Users.CreateAsync(user.AccountId, request);
            //            }
            //            catch (Exception ex) when (ex is BadRequestException || ex is TruliooException)
            //            {
            //                return new { error = ex.Message };
            //            }
            //
            //            return new { success = true };
            //        }
            //
            //   
            //        public async Task<dynamic> UpdateUser([Bind(Include = "UserGuid")]Models.UpdateUserRequest user)
            //        {
            //            user.Roles = (user.Roles ?? new List<string>()).Select(r => ((PortalUserRole)Int32.Parse(r)).ToString()).ToList();
            //            try
            //            {
            //                ValidateUser(user.Roles);
            //                await SessionProvider.ApiService.Configuration.Users.UpdateAsync(user.UserGuid, user);
            //            }
            //            catch (TruliooException ex)
            //            {
            //                return new { error = ex.Message };
            //            }
            //
            //            return new { success = true };
            //        }
            //
            //        private void ValidateUser(IEnumerable<string> roles)
            //        {
            //            if ((roles.Any(r => r == PortalUserRole.PortalUser.ToString()) && !SessionProvider.CurrentAccount.PortalAccess) ||
            //                (roles.Any(r => r == PortalUserRole.WebserviceUser.ToString()) && !SessionProvider.CurrentAccount.WebServiceAccess) ||
            //                (roles.Any(r => r == PortalUserRole.GGNormalizedAPIUser.ToString()) && !SessionProvider.CurrentAccount.NapiAccess))
            //            {
            //                throw new TruliooException("Unable to complete action: Insufficient account permissions for user type.");
            //            }
            //        }
            //
            //        [Route("doResetPassword")]
            //        [ValidateJsonAntiForgery]
            //        [HttpPost]
            //        public async Task<dynamic> DoResetPassword([Bind(Include = "AccountId, UserGuid, ChangePasswordUrl")]ChangeUserPasswordModel user)
            //        {
            //            await SessionProvider.ApiService.Configuration.Users.ResetPasswordAsync(user.UserGuid, new ResetPasswordRequest
            //            {
            //                ChangePasswordUrl = String.Format("https://{0}/account/changepassword",
            //                    getRequestUriHost(Request))
            //            });
            //
            //            return new { };
            //        }
            //
            //        [Route("getProfile")]
            //        [ValidateJsonAntiForgery]
            //        [HttpPost]
            //        public async Task<dynamic> GetProfile()
            //        {
            //            var user = await SessionProvider.ApiService.Configuration.Users.GetCurrentAsync();
            //            var userType = UserHelper.GeUserType(user.Roles);
            //
            //            var profile = new
            //            {
            //                creationDate = user.CreationDate,
            //                userName = SessionProvider.UserInfo.UserName,
            //                email = user.Email,
            //                status = user.Enabled ? "Enabled" : "Disabled",
            //                userType = UserHelper.GetDisplayableRoleName(userType),
            //                roles = UserHelper.GetUserRoles(user).Select(r => new { name = r.Name, isInRole = r.IsInRole }).ToArray()
            //            };
            //
            //            return new { profile };
            //        }
            //
            //        private string getRequestUriHost(HttpRequestMessage request)
            //        {
            //            return Request.RequestUri.Host == "localhost" ? "127.0.0.1" : Request.RequestUri.Host;
            //        }
        }
    }
}
