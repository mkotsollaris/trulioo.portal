﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using trulioo.portal.Configuration;
using trulioo.portal.DTO;
using Trulioo.Client.V2.Entities;

namespace WebApplication4.Controllers
{
    [Route("api/[controller]")]
    public class DatasourceGuideController : Controller
    {
        //     [HttpGet("[action]")]
        //     public async Task<IEnumerable<ScheduleTemplate>> GetAvailableDatasourceGuides()
        //     {
        //         try
        //         {
        //             var sdkConfiguration = new SDKConfiguration();
        //             var sdkClient = await sdkConfiguration.getService();
        //             List<ScheduleTemplate> availablesScheduleTemplates = await sdkClient.DatasourceGuide.GetAvailableScheduleTemplates();
        //             return availablesScheduleTemplates;
        //         }
        //         catch (Exception e)
        //         {
        //             throw e;
        //         }
        //     }

        //     [HttpGet("[action]")]
        //     public async Task<IEnumerable<DatasourceGuideDetailWithCountryCode>> GetMostRecentlyPublishedDatasourceGuide()
        //     {
        //         var sdkConfiguration = new SDKConfiguration();
        //         var sdkClient = await sdkConfiguration.getService();

        //         List<DatasourceGuideDetail> mostRecentlyPublishedDatasourceGuide = await sdkClient.DatasourceGuide.GetMostRecentlyPublishedDatasourceGuide();

        //         return mostRecentlyPublishedDatasourceGuide.Select(x => new DatasourceGuideDetailWithCountryCode
        //         {
        //             DatasourceID = x.DatasourceID,
        //             ProductDescription = x.ProductDescription,
        //             Country = x.Country,
        //             CountryCode = CountryEnumUtility.GetCountry(x.Country, true),
        //             DatasourceGroupName = x.DatasourceGroupName,
        //             Version = x.Version,
        //             PaymentPlan = x.PaymentPlan,
        //             Price = x.Price,
        //             DatasourceDetails = x.DatasourceDetails,
        //             DatasourceCountryDetails = x.DatasourceCountryDetails,
        //             DatasourceGroupCountryFields = x.DatasourceGroupCountryFields,
        //             AddressFormat = x.AddressFormat,
        //             NameFormat = x.NameFormat
        //         });
        //     }

        //     [Route("getavailablescheduletemplates")]
        //     [HttpGet("[action]")]
        //     public async Task<IEnumerable<ScheduleTemplate>> GetAvailableScheduleTemplates()
        //     {
        //         var sdkConfiguration = new SDKConfiguration();
        //         var sdkClient = await sdkConfiguration.getService();

        //         List<ScheduleTemplate> availablesScheduleTemplates = await sdkClient.DatasourceGuide.GetAvailableScheduleTemplates();
        //         return availablesScheduleTemplates;
        //     }

        //     [Route("getdatasourceguidebyscheduletemplateid/{scheduleTemplateId}")]
        //     [HttpGet("[action]")]
        //     public async Task<IEnumerable<DatasourceGuideDetailWithCountryCode>> GetDatasourceGuideByScheduleTemplateId(int scheduleTemplateId)
        //     {
        //         var sdkConfiguration = new SDKConfiguration();
        //         var sdkClient = await sdkConfiguration.getService();

        //         List<DatasourceGuideDetail> datasourceGuide = await sdkClient.DatasourceGuide.GetDatasourceGuideByScheduleTemplateId(scheduleTemplateId);

        //         return datasourceGuide.Select(x => new DatasourceGuideDetailWithCountryCode
        //         {
        //             DatasourceID = x.DatasourceID,
        //             ProductDescription = x.ProductDescription,
        //             Country = x.Country,
        //             CountryCode = CountryEnumUtility.GetCountry(x.Country, true),
        //             DatasourceGroupName = x.DatasourceGroupName,
        //             Version = x.Version,
        //             PaymentPlan = x.PaymentPlan,
        //             Price = x.Price,
        //             DatasourceDetails = x.DatasourceDetails,
        //             DatasourceCountryDetails = x.DatasourceCountryDetails,
        //             DatasourceGroupCountryFields = x.DatasourceGroupCountryFields,
        //             AddressFormat = x.AddressFormat,
        //             NameFormat = x.NameFormat
        //         });
        //     }
    }
}