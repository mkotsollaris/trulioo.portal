﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using trulioo.portal.Configuration;

namespace trulioo.portal.Controllers
{
    [Route("api/[controller]")]
    public class LoginController : Controller
    {
        private readonly IConfiguration _configuration;

        public LoginController(IConfiguration Configuration)
        {
            _configuration = Configuration;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Login([FromBody]LoginCredentialsDTO loginCredentials)
        {
            var sdkConfiguration = new SDKConfiguration(_configuration);
            var loginResult = await sdkConfiguration.Login(loginCredentials.username, loginCredentials.password);
            if (loginResult)
            {
                return Ok(loginResult);
            }
            return Ok();
        }
    }
}