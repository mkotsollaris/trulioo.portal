﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Trulioo.Client.V2;

namespace trulioo.portal.Configuration
{
    public class SDKConfiguration
    {
        private static Service sdkClient;
        private IConfiguration _configuration;
        private string clientId;
        private string clientSecret;
        private string gapiServiceURL;

        public SDKConfiguration(IConfiguration iConfig)
        {
            _configuration = iConfig;
            clientId = _configuration.GetSection("ClientId").Get<string>();
            clientSecret = _configuration.GetSection("ClientSecret").Get<string>();
            gapiServiceURL = _configuration.GetSection("GapiServiceURL").Get<string>();
        }

        public Service GetService()
        {
            if (sdkClient == null || sdkClient?.SessionKey == null)
            {
                //singleton
                sdkClient = new Service(new Context(gapiServiceURL));
                var apiService = new Service(gapiServiceURL);
                sdkClient.SessionKey = apiService.SessionKey;
            }
            return sdkClient;
        }

        public async Task<bool> Login(string username, string password)
        {
            try
            {
                if (sdkClient == null || sdkClient?.SessionKey == null)
                {
                    sdkClient = new Service(new Context(gapiServiceURL));

                    var apiService = new Service(gapiServiceURL);

                    await apiService.LogOnAsync(new TokenRequest
                    {
                        Username = username,
                        Password = password,
                        ClientId = clientId,
                        ClientSecret = clientSecret
                    }, CancellationToken.None);
                    sdkClient.SessionKey = apiService.SessionKey;
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}