﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace trulioo.portal.DTO
{
    public enum CountryEnum
    {
        [CountryInfo("Global", "")]
        Global = -1,

        [CountryInfo("Unknown", "")]
        Unknown = 0,
        /// <summary>
        /// Australia
        /// </summary>
        [CountryInfo("Australia", "AUS")]
        AU = 1,
        /// <summary>
        /// United States
        /// </summary>
        [CountryInfo("United States", "USA")]
        US = 2,
        /// <summary>
        /// Andorra
        /// </summary>
        [CountryInfo("Andorra", "AND")]
        AD = 3,
        /// <summary>
        /// United Arab Emirates
        /// </summary>
        [CountryInfo("United Arab Emirates", "ARE")]
        AE = 4,
        /// <summary>
        /// Afghanistan
        /// </summary>
        [CountryInfo("Afghanistan", "AFG")]
        AF = 5,
        /// <summary>
        /// Antigua and Barbuda
        /// </summary>
        [CountryInfo("Antigua and Barbuda", "ATG")]
        AG = 6,
        /// <summary>
        /// Anguilla
        /// </summary>
        [CountryInfo("Anguilla", "AIA")]
        AI = 7,
        /// <summary>
        /// Albania
        /// </summary>
        [CountryInfo("Albania", "ALB")]
        AL = 8,
        /// <summary>
        /// Armenia
        /// </summary>
        [CountryInfo("Armenia", "ARM")]
        AM = 9,
        /// <summary>
        /// Netherlands Antilles
        /// </summary>
        [CountryInfo("Netherlands Antilles", "ANT")]
        AN = 10,
        /// <summary>
        /// Angola
        /// </summary>
        [CountryInfo("Angola", "AGO")]
        AO = 11,
        /// <summary>
        /// Antarctica
        /// </summary>
        [CountryInfo("Antarctica", "ATA")]
        AQ = 12,
        /// <summary>
        /// Argentina
        /// </summary>
        [CountryInfo("Argentina", "ARG")]
        AR = 13,
        /// <summary>
        /// American Samoa
        /// </summary>
        [CountryInfo("American Samoa", "ASM")]
        AS = 14,
        /// <summary>
        /// Austria
        /// </summary>
        [CountryInfo("Austria", "AUT")]
        AT = 15,
        /// <summary>
        /// Aruba
        /// </summary>
        [CountryInfo("Aruba", "ABW")]
        AW = 16,
        /// <summary>
        /// Azerbaijan
        /// </summary>
        [CountryInfo("Azerbaijan", "AZE")]
        AZ = 17,
        /// <summary>
        /// Bosnia and Herzegovina
        /// </summary>
        [CountryInfo("Bosnia and Herzegovina", "BIH")]
        BA = 18,
        /// <summary>
        /// Barbados
        /// </summary>
        [CountryInfo("Barbados", "BRB")]
        BB = 19,
        /// <summary>
        /// Bangladesh
        /// </summary>
        [CountryInfo("Bangladesh", "BGD")]
        BD = 20,
        /// <summary>
        /// Belgium
        /// </summary>
        [CountryInfo("Belgium", "BEL")]
        BE = 21,
        /// <summary>
        /// Burkina Faso
        /// </summary>
        [CountryInfo("Burkina Faso", "BFA")]
        BF = 22,
        /// <summary>
        /// Bulgaria
        /// </summary>
        [CountryInfo("Bulgaria", "BGR")]
        BG = 23,
        /// <summary>
        /// Bahrain
        /// </summary>
        [CountryInfo("Bahrain", "BHR")]
        BH = 24,
        /// <summary>
        /// Burundi
        /// </summary>
        [CountryInfo("Burundi", "BDI")]
        BI = 25,
        /// <summary>
        /// Benin
        /// </summary>
        [CountryInfo("Benin", "BEN")]
        BJ = 26,
        /// <summary>
        /// Bermuda
        /// </summary>
        [CountryInfo("Bermuda", "BMU")]
        BM = 27,
        /// <summary>
        /// Brunei Darussalam
        /// </summary>
        [CountryInfo("Brunei Darussalam", "BRN")]
        BN = 28,
        /// <summary>
        /// Bolivia
        /// </summary>
        [CountryInfo("Bolivia", "BOL")]
        BO = 29,
        /// <summary>
        /// Brazil
        /// </summary>
        [CountryInfo("Brazil", "BRA")]
        BR = 30,
        /// <summary>
        /// Bahamas
        /// </summary>
        [CountryInfo("Bahamas", "BHS")]
        BS = 31,
        /// <summary>
        /// Bhutan
        /// </summary>
        [CountryInfo("Bhutan", "BTN")]
        BT = 32,
        /// <summary>
        /// Bouvet Island
        /// </summary>
        [CountryInfo("Bouvet Island", "BVT")]
        BV = 33,
        /// <summary>
        /// Botswana
        /// </summary>
        [CountryInfo("Botswana", "BWA")]
        BW = 34,
        /// <summary>
        /// Belarus
        /// </summary>
        [CountryInfo("Belarus", "BLR")]
        BY = 35,
        /// <summary>
        /// Belize
        /// </summary>
        [CountryInfo("Belize", "BLZ")]
        BZ = 36,
        /// <summary>
        /// Canada
        /// </summary>
        [CountryInfo("Canada", "CAN")]
        CA = 37,
        /// <summary>
        /// Cocos (Keeling) Islands
        /// </summary>
        [CountryInfo("Cocos (Keeling) Islands", "CCK")]
        CC = 38,
        /// <summary>
        /// The Democratic Republic of the Congo
        /// </summary>
        [CountryInfo("Congo, The Democratic Republic of the", "COD")]
        CD = 39,
        /// <summary>
        /// Central African Republic
        /// </summary>
        [CountryInfo("Central African Republic", "CAF")]
        CF = 40,
        /// <summary>
        /// Congo
        /// </summary>
        [CountryInfo("Congo", "COG")]
        CG = 41,
        /// <summary>
        /// Switzerland
        /// </summary>
        [CountryInfo("Switzerland", "CHE")]
        CH = 42,
        /// <summary>
        /// Cote D'Ivoire (Ivory Coast)
        /// </summary>
        [CountryInfo("Cote D'Ivoire (Ivory Coast)", "CIV")]
        CI = 43,
        /// <summary>
        /// Cook Islands
        /// </summary>
        [CountryInfo("Cook Islands", "COK")]
        CK = 44,
        /// <summary>
        /// Chile
        /// </summary>
        [CountryInfo("Chile", "CHL")]
        CL = 45,
        /// <summary>
        /// Cameroon
        /// </summary>
        [CountryInfo("Cameroon", "CMR")]
        CM = 46,
        /// <summary>
        /// China
        /// </summary>
        [CountryInfo("China", "CHN")]
        CN = 47,
        /// <summary>
        /// Colombia
        /// </summary>
        [CountryInfo("Colombia", "COL")]
        CO = 48,
        /// <summary>
        /// Costa Rica
        /// </summary>
        [CountryInfo("Costa Rica", "CRI")]
        CR = 49,
        /// <summary>
        /// Czechoslovakia
        /// </summary>
        [Obsolete]
        [CountryInfo("Czechoslovakia", "")]
        CS = 50,
        /// <summary>
        /// Cuba
        /// </summary>
        [CountryInfo("Cuba", "CUB")]
        CU = 51,
        /// <summary>
        /// Cape Verde
        /// </summary>
        [CountryInfo("Cape Verde", "CPV")]
        CV = 52,
        /// <summary>
        /// Curaçao
        /// </summary>
        [CountryInfo("Curaçao", "CUW")]
        CW = 242,
        /// <summary>
        /// Christmas Island
        /// </summary>
        [CountryInfo("Christmas Island", "CXR")]
        CX = 53,
        /// <summary>
        /// Cyprus
        /// </summary>
        [CountryInfo("Cyprus", "CYP")]
        CY = 54,
        /// <summary>
        /// Czech Republic
        /// </summary>
        [CountryInfo("Czech Republic", "CZE")]
        CZ = 55,
        /// <summary>
        /// Germany
        /// </summary>
        [CountryInfo("Germany", "DEU")]
        DE = 56,
        /// <summary>
        /// Djibouti
        /// </summary>
        [CountryInfo("Djibouti", "DJI")]
        DJ = 57,
        /// <summary>
        /// Denmark
        /// </summary>
        [CountryInfo("Denmark", "DNK")]
        DK = 58,
        /// <summary>
        /// Dominica
        /// </summary>
        [CountryInfo("Dominica", "DMA")]
        DM = 59,
        /// <summary>
        /// Dominican Republic
        /// </summary>
        [CountryInfo("Dominican Republic", "DOM")]
        DO = 60,
        /// <summary>
        /// Algeria
        /// </summary>
        [CountryInfo("Algeria", "DZA")]
        DZ = 61,
        /// <summary>
        /// Ecuador
        /// </summary>
        [CountryInfo("Ecuador", "ECU")]
        EC = 62,
        /// <summary>
        /// Estonia
        /// </summary>
        [CountryInfo("Estonia", "EST")]
        EE = 63,
        /// <summary>
        /// Egypt
        /// </summary>
        [CountryInfo("Egypt", "EGY")]
        EG = 64,
        /// <summary>
        /// Western Sahara
        /// </summary>
        [CountryInfo("Western Sahara", "ESH")]
        EH = 65,
        /// <summary>
        /// Eritrea
        /// </summary>
        [CountryInfo("Eritrea", "ERI")]
        ER = 66,
        /// <summary>
        /// Spain
        /// </summary>
        [CountryInfo("Spain", "ESP")]
        ES = 67,
        /// <summary>
        /// Ethiopia
        /// </summary>
        [CountryInfo("Ethiopia", "ETH")]
        ET = 68,
        /// <summary>
        /// Finland
        /// </summary>
        [CountryInfo("Finland", "FIN")]
        FI = 69,
        /// <summary>
        /// Fiji
        /// </summary>
        [CountryInfo("Fiji", "FJI")]
        FJ = 70,
        /// <summary>
        /// Falkland Islands (Malvinas)
        /// </summary>
        [CountryInfo("Falkland Islands (Malvinas)", "FLK")]
        FK = 71,
        /// <summary>
        /// Federated States of Micronesia
        /// </summary>
        [CountryInfo("Micronesia, Federated States of", "FSM")]
        FM = 72,
        /// <summary>
        /// Faroe Islands
        /// </summary>
        [CountryInfo("Faroe Islands", "FRO")]
        FO = 73,
        /// <summary>
        /// France
        /// </summary>
        [CountryInfo("France", "FRA")]
        FR = 74,
        /// <summary>
        /// Gabon
        /// </summary>
        [CountryInfo("Gabon", "GAB")]
        GA = 75,
        /// <summary>
        /// United Kingdom
        /// </summary>
        [CountryInfo("United Kingdom", "GBR")]
        GB = 224,
        /// <summary>
        /// Grenada
        /// </summary>
        [CountryInfo("Grenada", "GRD")]
        GD = 76,
        /// <summary>
        /// Georgia
        /// </summary>
        [CountryInfo("Georgia", "GEO")]
        GE = 77,
        /// <summary>
        /// French Guiana
        /// </summary>
        [CountryInfo("French Guiana", "GUF")]
        GF = 78,
        /// <summary>
        /// Ghana
        /// </summary>
        [CountryInfo("Ghana", "GHA")]
        GH = 79,
        /// <summary>
        /// Gibraltar
        /// </summary>
        [CountryInfo("Gibraltar", "GIB")]
        GI = 80,
        /// <summary>
        /// Greenland
        /// </summary>
        [CountryInfo("Greenland", "GRL")]
        GL = 81,
        /// <summary>
        /// Gambia
        /// </summary>
        [CountryInfo("Gambia", "GMB")]
        GM = 82,
        /// <summary>
        /// Guinea
        /// </summary>
        [CountryInfo("Guinea", "GIN")]
        GN = 83,
        /// <summary>
        /// Guadeloupe
        /// </summary>
        [CountryInfo("Guadeloupe", "GLP")]
        GP = 84,
        /// <summary>
        /// Equatorial Guinea
        /// </summary>
        [CountryInfo("Equatorial Guinea", "GNQ")]
        GQ = 85,
        /// <summary>
        /// Greece
        /// </summary>
        [CountryInfo("Greece", "GRC")]
        GR = 86,
        /// <summary>
        /// South Georgia and the South Sandwich Islands
        /// </summary>
        [CountryInfo("South Georgia and the South Sandwich Islands", "SGS")]
        GS = 87,
        /// <summary>
        /// Guatemala
        /// </summary>
        [CountryInfo("Guatemala", "GTM")]
        GT = 88,
        /// <summary>
        /// Guam
        /// </summary>
        [CountryInfo("Guam", "GUM")]
        GU = 89,
        /// <summary>
        /// Guinea-Bissau
        /// </summary>
        [CountryInfo("Guinea-Bissau", "GNB")]
        GW = 90,
        /// <summary>
        /// Guyana
        /// </summary>
        [CountryInfo("Guyana", "GUY")]
        GY = 91,
        /// <summary>
        /// Hong Kong
        /// </summary>
        [CountryInfo("Hong Kong", "HKG")]
        HK = 92,
        /// <summary>
        /// Heard and McDonald Islands
        /// </summary>
        [CountryInfo("Heard and McDonald Islands", "HMD")]
        HM = 93,
        /// <summary>
        /// Honduras
        /// </summary>
        [CountryInfo("Honduras", "HND")]
        HN = 94,
        /// <summary>
        /// Croatia
        /// </summary>
        [CountryInfo("Croatia", "HRV")]
        HR = 95,
        /// <summary>
        /// Haiti
        /// </summary>
        [CountryInfo("Haiti", "HTI")]
        HT = 96,
        /// <summary>
        /// Hungary
        /// </summary>
        [CountryInfo("Hungary", "HUN")]
        HU = 97,
        /// <summary>
        /// Indonesia
        /// </summary>
        [CountryInfo("Indonesia", "IDN")]
        ID = 98,
        /// <summary>
        /// Ireland
        /// </summary>
        [CountryInfo("Ireland", "IRL")]
        IE = 99,
        /// <summary>
        /// Israel
        /// </summary>
        [CountryInfo("Israel", "ISR")]
        IL = 100,
        /// <summary>
        /// India
        /// </summary>
        [CountryInfo("India", "IND")]
        IN = 101,
        /// <summary>
        /// British Indian Ocean Territory
        /// </summary>
        [CountryInfo("British Indian Ocean Territory", "IOT")]
        IO = 102,
        /// <summary>
        /// Iraq
        /// </summary>
        [CountryInfo("Iraq", "IRQ")]
        IQ = 103,
        /// <summary>
        /// Iran
        /// </summary>
        [CountryInfo("Iran, Islamic Republic of", "IRN")]
        IR = 104,
        /// <summary>
        /// Iceland
        /// </summary>
        [CountryInfo("Iceland", "ISL")]
        IS = 105,
        /// <summary>
        /// Italy
        /// </summary>
        [CountryInfo("Italy", "ITA")]
        IT = 106,
        /// <summary>
        /// Jersey
        /// </summary>
        [CountryInfo("Jersey", "JEY")]
        JE = 107,
        /// <summary>
        /// Jamaica
        /// </summary>
        [CountryInfo("Jamaica", "JAM")]
        JM = 108,
        /// <summary>
        /// Jordan
        /// </summary>
        [CountryInfo("Jordan", "JOR")]
        JO = 109,
        /// <summary>
        /// Japan
        /// </summary>
        [CountryInfo("Japan", "JPN")]
        JP = 110,
        /// <summary>
        /// Kenya
        /// </summary>
        [CountryInfo("Kenya", "KEN")]
        KE = 111,
        /// <summary>
        /// Kyrgyzstan
        /// </summary>
        [CountryInfo("Kyrgyzstan", "KGZ")]
        KG = 112,
        /// <summary>
        /// Cambodia
        /// </summary>
        [CountryInfo("Cambodia", "KHM")]
        KH = 113,
        /// <summary>
        /// Kiribati
        /// </summary>
        [CountryInfo("Kiribati", "KIR")]
        KI = 114,
        /// <summary>
        /// Comoros
        /// </summary>
        [CountryInfo("Comoros", "COM")]
        KM = 115,
        /// <summary>
        /// Saint Kitts and Nevis
        /// </summary>
        [CountryInfo("Saint Kitts and Nevis", "KNA")]
        KN = 116,
        /// <summary>
        /// Korea (North)
        /// </summary>
        [CountryInfo("Korea, Democratic People's Republic of", "PRK")]
        KP = 117,
        /// <summary>
        /// Korea (South)
        /// </summary>
        [CountryInfo("Korea, Republic of", "KOR")]
        KR = 118,
        /// <summary>
        /// Kuwait
        /// </summary>
        [CountryInfo("Kuwait", "KWT")]
        KW = 119,
        /// <summary>
        /// Cayman Islands
        /// </summary>
        [CountryInfo("Cayman Islands", "CYM")]
        KY = 120,
        /// <summary>
        /// Kazakhstan
        /// </summary>
        [CountryInfo("Kazakhstan", "KAZ")]
        KZ = 121,
        /// <summary>
        /// Laos
        /// </summary>
        [CountryInfo("Lao People's Democratic Republic", "LAO")]
        LA = 122,
        /// <summary>
        /// Lebanon
        /// </summary>
        [CountryInfo("Lebanon", "LBN")]
        LB = 123,
        /// <summary>
        /// Saint Lucia
        /// </summary>
        [CountryInfo("Saint Lucia", "LCA")]
        LC = 124,
        /// <summary>
        /// Liechtenstein
        /// </summary>
        [CountryInfo("Liechtenstein", "LIE")]
        LI = 125,
        /// <summary>
        /// Sri Lanka
        /// </summary>
        [CountryInfo("Sri Lanka", "LKA")]
        LK = 126,
        /// <summary>
        /// Liberia
        /// </summary>
        [CountryInfo("Liberia", "LBR")]
        LR = 127,
        /// <summary>
        /// Lesotho
        /// </summary>
        [CountryInfo("Lesotho", "LSO")]
        LS = 128,
        /// <summary>
        /// Lithuania
        /// </summary>
        [CountryInfo("Lithuania", "LTU")]
        LT = 129,
        /// <summary>
        /// Luxembourg
        /// </summary>
        [CountryInfo("Luxembourg", "LUX")]
        LU = 130,
        /// <summary>
        /// Latvia
        /// </summary>
        [CountryInfo("Latvia", "LVA")]
        LV = 131,
        /// <summary>
        /// Libya
        /// </summary>
        [CountryInfo("Libya", "LBY")]
        LY = 132,
        /// <summary>
        /// Morocco
        /// </summary>
        [CountryInfo("Morocco", "MAR")]
        MA = 133,
        /// <summary>
        /// SAINT MARTIN (FRENCH PART)
        /// </summary>
        [CountryInfo("Saint Martin (French part)", "MAF")]
        MF = 134,
        /// <summary>
        /// Monaco
        /// </summary>
        [CountryInfo("Monaco", "MCO")]
        MC = 135,
        /// <summary>
        /// Moldova
        /// </summary>
        [CountryInfo("Moldova, Republic of", "MDA")]
        MD = 136,
        /// <summary>
        /// Madagascar
        /// </summary>
        [CountryInfo("Madagascar", "MDG")]
        MG = 137,
        /// <summary>
        /// Marshall Islands
        /// </summary>
        [CountryInfo("Marshall Islands", "MHL")]
        MH = 138,
        /// <summary>
        /// Macedonia
        /// </summary>
        [CountryInfo("Macedonia, Republic of", "MKD")]
        MK = 139,
        /// <summary>
        /// Mali
        /// </summary>
        [CountryInfo("Mali", "MLI")]
        ML = 140,
        /// <summary>
        /// Myanmar
        /// </summary>
        [CountryInfo("Myanmar", "MMR")]
        MM = 141,
        /// <summary>
        /// Mongolia
        /// </summary>
        [CountryInfo("Mongolia", "MNG")]
        MN = 142,
        /// <summary>
        /// Macao
        /// </summary>
        [CountryInfo("Macao", "MAC")]
        MO = 143,
        /// <summary>
        /// Northern Mariana Islands
        /// </summary>
        [CountryInfo("Northern Mariana Islands", "MNP")]
        MP = 144,
        /// <summary>
        /// Martinique
        /// </summary>
        [CountryInfo("Martinique", "MTQ")]
        MQ = 145,
        /// <summary>
        /// Mauritania
        /// </summary>
        [CountryInfo("Mauritania", "MRT")]
        MR = 146,
        /// <summary>
        /// Montserrat
        /// </summary>
        [CountryInfo("Montserrat", "MSR")]
        MS = 147,
        /// <summary>
        /// Malta
        /// </summary>
        [CountryInfo("Malta", "MLT")]
        MT = 148,
        /// <summary>
        /// Mauritius
        /// </summary>
        [CountryInfo("Mauritius", "MUS")]
        MU = 149,
        /// <summary>
        /// Maldives
        /// </summary>
        [CountryInfo("Maldives", "MDV")]
        MV = 150,
        /// <summary>
        /// Malawi
        /// </summary>
        [CountryInfo("Malawi", "MWI")]
        MW = 151,
        /// <summary>
        /// Mexico
        /// </summary>
        [CountryInfo("Mexico", "MEX")]
        MX = 152,
        /// <summary>
        /// Malaysia
        /// </summary>
        [CountryInfo("Malaysia", "MYS")]
        MY = 153,
        /// <summary>
        /// Mozambique
        /// </summary>
        [CountryInfo("Mozambique", "MOZ")]
        MZ = 154,
        /// <summary>
        /// Namibia
        /// </summary>
        [CountryInfo("Namibia", "NAM")]
        NA = 155,
        /// <summary>
        /// New Caledonia
        /// </summary>
        [CountryInfo("New Caledonia", "NCL")]
        NC = 156,
        /// <summary>
        /// Niger
        /// </summary>
        [CountryInfo("Niger", "NER")]
        NE = 157,
        /// <summary>
        /// Norfolk Island
        /// </summary>
        [CountryInfo("Norfolk Island", "NFK")]
        NF = 158,
        /// <summary>
        /// Nigeria
        /// </summary>
        [CountryInfo("Nigeria", "NGA")]
        NG = 159,
        /// <summary>
        /// Nicaragua
        /// </summary>
        [CountryInfo("Nicaragua", "NIC")]
        NI = 160,
        /// <summary>
        /// Netherlands
        /// </summary>
        [CountryInfo("Netherlands", "NLD")]
        NL = 161,
        /// <summary>
        /// Norway
        /// </summary>
        [CountryInfo("Norway", "NOR")]
        NO = 162,
        /// <summary>
        /// Nepal
        /// </summary>
        [CountryInfo("Nepal", "NPL")]
        NP = 163,
        /// <summary>
        /// Nauru
        /// </summary>
        [CountryInfo("Nauru", "NRU")]
        NR = 164,
        /// <summary>
        /// Niue
        /// </summary>
        [CountryInfo("Niue", "NIU")]
        NU = 165,
        /// <summary>
        /// New Zealand
        /// </summary>
        [CountryInfo("New Zealand", "NZL")]
        NZ = 166,
        /// <summary>
        /// Oman
        /// </summary>
        [CountryInfo("Oman", "OMN")]
        OM = 167,
        /// <summary>
        /// Panama
        /// </summary>
        [CountryInfo("Panama", "PAN")]
        PA = 168,
        /// <summary>
        /// Peru
        /// </summary>
        [CountryInfo("Peru", "PER")]
        PE = 169,
        /// <summary>
        /// French Polynesia
        /// </summary>
        [CountryInfo("French Polynesia", "PYF")]
        PF = 170,
        /// <summary>
        /// Papua New Guinea
        /// </summary>
        [CountryInfo("Papua New Guinea", "PNG")]
        PG = 171,
        /// <summary>
        /// Philippines
        /// </summary>
        [CountryInfo("Philippines", "PHL")]
        PH = 172,
        /// <summary>
        /// Pakistan
        /// </summary>
        [CountryInfo("Pakistan", "PAK")]
        PK = 173,
        /// <summary>
        /// Poland
        /// </summary>
        [CountryInfo("Poland", "POL")]
        PL = 174,
        /// <summary>
        /// St. Pierre and Miquelon
        /// </summary>
        [CountryInfo("Saint Pierre and Miquelon", "SPM")]
        PM = 175,
        /// <summary>
        /// Pitcairn
        /// </summary>
        [CountryInfo("Pitcairn", "PCN")]
        PN = 176,
        /// <summary>
        /// Puerto Rico
        /// </summary>
        [CountryInfo("Puerto Rico", "PRI")]
        PR = 177,
        /// <summary>
        /// Palestine
        /// </summary>
        [CountryInfo("Palestine", "PSE")]
        PS = 178,
        /// <summary>
        /// Portugal
        /// </summary>
        [CountryInfo("Portugal", "PRT")]
        PT = 179,
        /// <summary>
        /// Palau
        /// </summary>
        [CountryInfo("Palau", "PLW")]
        PW = 180,
        /// <summary>
        /// Paraguay
        /// </summary>
        [CountryInfo("Paraguay", "PRY")]
        PY = 181,
        /// <summary>
        /// Qatar
        /// </summary>
        [CountryInfo("Qatar", "QAT")]
        QA = 182,
        /// <summary>
        /// Reunion
        /// </summary>
        [CountryInfo("Reunion", "REU")]
        RE = 183,
        /// <summary>
        /// Romania
        /// </summary>
        [CountryInfo("Romania", "ROU")]
        RO = 184,
        /// <summary>
        /// Serbia
        /// </summary>
        [CountryInfo("Serbia", "SRB")]
        RS = 246,
        /// <summary>
        /// Russian Federation
        /// </summary>
        [CountryInfo("Russian Federation", "RUS")]
        RU = 185,
        /// <summary>
        /// Rwanda
        /// </summary>
        [CountryInfo("Rwanda", "RWA")]
        RW = 186,
        /// <summary>
        /// Saudi Arabia
        /// </summary>
        [CountryInfo("Saudi Arabia", "SAU")]
        SA = 187,
        /// <summary>
        /// Solomon Islands
        /// </summary>
        [CountryInfo("Solomon Islands", "SLB")]
        SB = 188,
        /// <summary>
        /// Seychelles
        /// </summary>
        [CountryInfo("Seychelles", "SYC")]
        SC = 189,
        /// <summary>
        /// Sudan
        /// </summary>
        [CountryInfo("Sudan", "SDN")]
        SD = 190,
        /// <summary>
        /// Sweden
        /// </summary>
        [CountryInfo("Sweden", "SWE")]
        SE = 191,
        /// <summary>
        /// Singapore
        /// </summary>
        [CountryInfo("Singapore", "SGP")]
        SG = 192,
        /// <summary>
        /// St. Helena
        /// </summary>
        [CountryInfo("Saint Helena", "SHN")]
        SH = 193,
        /// <summary>
        /// Slovenia
        /// </summary>
        [CountryInfo("Slovenia", "SVN")]
        SI = 194,
        /// <summary>
        /// Svalbard and Jan Mayen Islands
        /// </summary>
        [CountryInfo("Svalbard and Jan Mayen Islands", "SJM")]
        SJ = 195,
        /// <summary>
        /// Slovak Republic
        /// </summary>
        [CountryInfo("Slovakia", "SVK")]
        SK = 196,
        /// <summary>
        /// Sierra Leone
        /// </summary>
        [CountryInfo("Sierra Leone", "SLE")]
        SL = 197,
        /// <summary>
        /// San Marino
        /// </summary>
        [CountryInfo("San Marino", "SMR")]
        SM = 198,
        /// <summary>
        /// Senegal
        /// </summary>
        [CountryInfo("Senegal", "SEN")]
        SN = 199,
        /// <summary>
        /// Somalia
        /// </summary>
        [CountryInfo("Somalia", "SOM")]
        SO = 200,
        /// <summary>
        /// Suriname
        /// </summary>
        [CountryInfo("Suriname", "SUR")]
        SR = 201,
        /// <summary>
        /// Sao Tome and Principe
        /// </summary>
        [CountryInfo("Sao Tome and Principe", "STP")]
        ST = 202,
        /// <summary>
        /// El Salvador
        /// </summary>
        [CountryInfo("El Salvador", "SLV")]
        SV = 203,
        /// <summary>
        /// Syria
        /// </summary>
        [CountryInfo("Syria", "SYR")]
        SY = 204,
        /// <summary>
        /// Swaziland
        /// </summary>
        [CountryInfo("Swaziland", "SWZ")]
        SZ = 205,
        /// <summary>
        /// Turks and Caicos Islands
        /// </summary>
        [CountryInfo("Turks and Caicos Islands", "TCA")]
        TC = 206,
        /// <summary>
        /// Chad
        /// </summary>
        [CountryInfo("Chad", "TCD")]
        TD = 207,
        /// <summary>
        /// French Southern Territories
        /// </summary>
        [CountryInfo("French Southern Territories", "ATF")]
        TF = 208,
        /// <summary>
        /// Togo
        /// </summary>
        [CountryInfo("Togo", "TGO")]
        TG = 209,
        /// <summary>
        /// Thailand
        /// </summary>
        [CountryInfo("Thailand", "THA")]
        TH = 210,
        /// <summary>
        /// Tajikistan
        /// </summary>
        [CountryInfo("Tajikistan", "TJK")]
        TJ = 211,
        /// <summary>
        /// Tokelau
        /// </summary>
        [CountryInfo("Tokelau", "TKL")]
        TK = 212,
        /// <summary>
        /// TIMOR-LESTE
        /// </summary>
        [CountryInfo("Timor-Leste", "TLS")]
        TL = 213,
        /// <summary>
        /// Turkmenistan
        /// </summary>
        [CountryInfo("Turkmenistan", "TKM")]
        TM = 214,
        /// <summary>
        /// Tunisia
        /// </summary>
        [CountryInfo("Tunisia", "TUN")]
        TN = 215,
        /// <summary>
        /// Tonga
        /// </summary>
        [CountryInfo("Tonga", "TON")]
        TO = 216,
        /// <summary>
        /// Turkey
        /// </summary>
        [CountryInfo("Turkey", "TUR")]
        TR = 217,
        /// <summary>
        /// Trinidad and Tobago
        /// </summary>
        [CountryInfo("Trinidad and Tobago", "TTO")]
        TT = 218,
        /// <summary>
        /// Tuvalu
        /// </summary>
        [CountryInfo("Tuvalu", "TUV")]
        TV = 219,
        /// <summary>
        /// Taiwan
        /// </summary>
        [CountryInfo("Taiwan", "TWN")]
        TW = 220,
        /// <summary>
        /// Tanzania
        /// </summary>
        [CountryInfo("Tanzania", "TZA")]
        TZ = 221,
        /// <summary>
        /// Ukraine
        /// </summary>
        [CountryInfo("Ukraine", "UKR")]
        UA = 222,
        /// <summary>
        /// Uganda
        /// </summary>
        [CountryInfo("Uganda", "UGA")]
        UG = 223,
        /// <summary>
        /// US Minor Outlying Islands
        /// </summary>
        [CountryInfo("US Minor Outlying Islands", "UMI")]
        UM = 225,
        /// <summary>
        /// Uruguay
        /// </summary>
        [CountryInfo("Uruguay", "URY")]
        UY = 226,
        /// <summary>
        /// Uzbekistan
        /// </summary>
        [CountryInfo("Uzbekistan", "UZB")]
        UZ = 227,
        /// <summary>
        /// Vatican City State (Holy See)
        /// </summary>
        [CountryInfo("Vatican City State (Holy See)", "VAT")]
        VA = 228,
        /// <summary>
        /// Saint Vincent and The Grenadines
        /// </summary>
        [CountryInfo("Saint Vincent and the Grenadines", "VCT")]
        VC = 229,
        /// <summary>
        /// Venezuela
        /// </summary>
        [CountryInfo("Venezuela", "VEN")]
        VE = 230,
        /// <summary>
        /// Virgin Islands (British)
        /// </summary>
        [CountryInfo("Virgin Islands, British", "VGB")]
        VG = 231,
        /// <summary>
        /// Virgin Islands (US)
        /// </summary>
        [CountryInfo("Virgin Islands, US", "VIR")]
        VI = 232,
        /// <summary>
        /// Viet Nam
        /// </summary>
        [CountryInfo("Viet Nam", "VNM")]
        VN = 233,
        /// <summary>
        /// Vanuatu
        /// </summary>
        [CountryInfo("Vanuatu", "VUT")]
        VU = 234,
        /// <summary>
        /// Wallis and Futuna Islands
        /// </summary>
        [CountryInfo("Wallis and Futuna Islands", "WLF")]
        WF = 235,
        /// <summary>
        /// Samoa
        /// </summary>
        [CountryInfo("Samoa", "WSM")]
        WS = 236,
        /// <summary>
        /// Yemen
        /// </summary>
        [CountryInfo("Yemen", "YEM")]
        YE = 237,
        /// <summary>
        /// Mayotte
        /// </summary>
        [CountryInfo("Mayotte", "MYT")]
        YT = 238,
        /// <summary>
        /// South Africa
        /// </summary>
        [CountryInfo("South Africa", "ZAF")]
        ZA = 239,
        /// <summary>
        /// Zambia
        /// </summary>
        [CountryInfo("Zambia", "ZMB")]
        ZM = 240,
        /// <summary>
        /// Zimbabwe
        /// </summary>
        [CountryInfo("Zimbabwe", "ZWE")]
        ZW = 241,
        /// <summary>
        ///  Sint Maarten
        /// </summary>
        [CountryInfo("Sint Maarten", "SXM")]
        SX = 243,
        /// <summary>
        /// Caribbean Netherlands ( Bonaire, Sint Eustatius, and Saba)
        /// </summary>
        [CountryInfo("Caribbean Netherlands", "BES")]
        BQ = 244,
        /// <summary>
        /// Montenegro
        /// </summary>
        [CountryInfo("Montenegro", "MNE")]
        ME = 245,
        /// <summary>
        /// South Sudan
        /// </summary>
        [CountryInfo("South Sudan", "SSD")]
        SS = 247,
        [CountryInfo("Guernsey", "GGY")]
        GG = 248,
        [CountryInfo("Isle of Man", "IMN")]
        IM = 249,
        /// <summary>
        /// Kosovo
        /// </summary>
        [CountryInfo("Kosovo", "XKX")]
        XK = 250,
    }

    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class CountryInfoAttribute : Attribute
    {
        public readonly string CountryName;
        public readonly string Alpha3;
        public CountryInfoAttribute(string countryName, string alpha3)
        {
            CountryName = countryName;
            Alpha3 = alpha3;
        }
    }

    public static class CountryEnumUtility
    {
        private static IReadOnlyDictionary<string, CountryEnum> _nameToCoutryCountryEnum = Enum.GetValues(typeof(CountryEnum)).Cast<CountryEnum>().ToDictionary(x => GetCountryName(x).ToLower(), x => x);
        [Obsolete]
        private static IReadOnlyDictionary<string, CountryEnum> _alpha3ToCountryEnum = Enum.GetValues(typeof(CountryEnum)).Cast<CountryEnum>().Except(new[] { CountryEnum.Global, CountryEnum.Unknown, CountryEnum.CS }).ToDictionary(x => GetAlpha3Code(x), x => x, StringComparer.OrdinalIgnoreCase);

        public static string GetCountryName(CountryEnum countryEnum)
        {
            var customAttributes = typeof(CountryEnum).GetField(countryEnum.ToString()).GetCustomAttributes(typeof(CountryInfoAttribute), false);
            if (customAttributes.Length == 1)
            {
                return ((CountryInfoAttribute)customAttributes[0]).CountryName;
            }
            else
            {
                throw new Exception("Whoa! That shouldn't happen!");
            }
        }
        public static string GetCountryName(int countryId)
        {
            return GetCountryName((CountryEnum)countryId);
        }
        public static string GetCountryName(string countryCode)
        {
            return GetCountryName(Alpha2ToEnum(countryCode));
        }

        public static CountryEnum Alpha2ToEnum(string countryCode)
        {
            CountryEnum result;
            return Enum.TryParse<CountryEnum>(value: countryCode, ignoreCase: true, result: out result) ? result : CountryEnum.Unknown;
        }

        public static string GetAlpha3Code(CountryEnum countryEnum)
        {
            var customAttributes = typeof(CountryEnum).GetField(countryEnum.ToString()).GetCustomAttributes(typeof(CountryInfoAttribute), false);
            if (customAttributes.Length == 1)
            {
                return ((CountryInfoAttribute)customAttributes[0]).Alpha3;
            }
            else
            {
                throw new Exception("Whoa! That shouldn't happen!");
            }
        }

        public static string GetCountry(string countryValue, bool getAlpha2)
        {
            if (string.IsNullOrEmpty(countryValue))
            {
                return null;
            }

            CountryEnum result = Alpha2ToEnum(countryValue);
            if (result == CountryEnum.Unknown)
            {
                _nameToCoutryCountryEnum.TryGetValue(countryValue.ToLower(), out result);
            }

            if (result == CountryEnum.Unknown)
            {
                return null;
            }

            return getAlpha2 ? result.ToString() : GetCountryName(result);
        }

        public static IEnumerable<CountryEnum> GetCountries()
        {
            return Enum.GetValues(typeof(CountryEnum)).Cast<CountryEnum>().Where(c => c != CountryEnum.Unknown);
        }

        public static CountryEnum GetCountryEnumFromAlpha3(string alpha3)
        {
            if (_alpha3ToCountryEnum.TryGetValue(alpha3, out var result))
            {
                return result;
            }
            return CountryEnum.Unknown;
        }
    }
}
