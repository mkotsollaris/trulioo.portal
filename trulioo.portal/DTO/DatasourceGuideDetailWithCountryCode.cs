﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Trulioo.Client.V2.Entities;

namespace trulioo.portal.DTO
{
    public class DatasourceGuideDetailWithCountryCode : DatasourceGuideDetail
    {
        public string CountryCode { get; set; }
    }
}
